class CallbacksController < ApplicationController

  skip_before_action :authenticate_user!, only: [ :portOutValidationCallbackApi ] 
  protect_from_forgery with: :null_session
  respond_to :xml

  def show()
    @account = Account.find(params[:id])
    respond_with do |format|
      format.html
      format.xml { render :xml =>
                   @account.to_xml(
                   :camelize => true,
                   :methods => "PON",
                   :except => [:pon, :created_at, :updated_at ] ),
                   :layout => false
      }
    end
  end

  def portOutValidationCallbackApi()
    @request = request.body.read

    logger = ActiveSupport::TaggedLogging.new(Logger.new("log/transactions.log"))
    time = Time.now
    logger.tagged("") { logger.info "[Request] ||||||||||||||||||||||||||||||||| #{time}" }
    logger.tagged("") { logger.info @request }

    @validation_service = PortOutValidationRequestService.new @request
    @validation_response = PortOutValidationResponse.new

    if @validation_service.errors.count > 0
      @validation_service.errors.each do |code|
        @validation_response.add_error code
      end
    else
      if Account.exists?(account_number: @validation_service.field("AccountNumber"))
        @account = Account.where(account_number: @validation_service.field("AccountNumber")).first
        @validation_response.portable = true 
        @validation_response.pon = @validation_service.field("PON")
      else
        @validation_response.add_error PortOutValidationRequestService::ERROR_INVALID_ACCOUNT_CODE
      end
    end

    logger.tagged("") { logger.info "[Response] |||||||||||||||||||||||||||||||||" }
    logger.tagged("") { logger.info @validation_response.to_xml() }
    logger.tagged("") { logger.info "|||||||||||||||||||||||||||||||||[End]" }

    response.headers['Connection'] = 'Closed'
    remove_keys = %w(X-Runtime Cache-Control Server Etag Set-Cookie)
    response.headers.delete_if{|key| remove_keys.include? key}

    respond_with do |format|
      format.xml { 
                  render :xml => @validation_response.to_xml(),
                  :layout => false
      }
    end
  end

  def index()

    render "None"

  end

end
